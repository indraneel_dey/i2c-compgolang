package mpu6050

import (
	"strconv"
	"strings"
)

//BEGIN: Accelerometer Bandwidth enum
type AccConfigBandwidth uint8
const (
	AccB260 AccConfigBandwidth = 1+iota
	AccB184
	AccB94
	AccB44
	AccB21
	AccB10
	AccB5
)
var accBandwidths = [...]string{
	"260",
	"184",
	"94",
	"44",
	"21",
	"10",
	"5",
}
func (ab AccConfigBandwidth) String() string { return accBandwidths[ac-1] }
//END: Accelerometer Bandwidth enum

//BEGIN: Gyro Bandwidth enum
type GyroAConfigBandwidth int
const(
	GyroB256 GyroAConfigBandwidth = 1+ iota
	GyroB188
	GyroB98
	GyroB42
	GyroB20
	GyroB10
	GryoB5
)
var gyroBandwidths = [...]string{
	"256",
	"188",
	"98",
	"42",
	"20",
	"10",
	"5",
}
func (gb GyroAConfigBandwidth) String() string { return gyroBandwidths[gb-1] }
//END: Gyro Bandwidth enum

//Reads the config bandwidth register
func (e MPU6050) readConfigBandwidth() int{
	var regValue uint8
	regValue, e.Error = e.Connection.ReadRegU8(byte(config))
	runes := []rune(strconv.FormatUint(uint64(regValue),2))

	b := make([]byte, 0)
	for _, s := range strings.Fields(string(runes[5:7])) {
		n, _ := strconv.ParseUint(s, 2, 3)
		b = append(b, byte(n))
	}

	var data int
	data, e.Error = strconv.Atoi(string(b))

	return data
}

func (e MPU6050) ReadAccConfigBandwidth() AccConfigBandwidth{
	return AccConfigBandwidth(e.readConfigBandwidth())
}

func (e MPU6050) ReadGyroConfigBandwidth() GyroAConfigBandwidth{
	return GyroAConfigBandwidth(e.readConfigBandwidth())
}