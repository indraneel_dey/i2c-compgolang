package mpu6050

import (
	"fmt"
)

func (e MPU6050) RunTests(){
	var xreg,yreg,zreg uint8

	xreg, e.Error = e.Connection.ReadRegU8(byte(self_test_x))
	yreg, e.Error = e.Connection.ReadRegU8(byte(self_test_y))
	zreg, e.Error = e.Connection.ReadRegU8(byte(self_test_z))

	if e.Error!=nil{
		fmt.Printf("Error: %v", e.Error)
	}

	fmt.Printf("X: %v\nY: %v\nZ: %v", xreg, yreg, zreg)
}
