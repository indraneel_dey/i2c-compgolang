package mpu6050

import (
	"fmt"
	"github.com/d2r2/go-i2c"
)

type MPU6050 struct{
	//X Y X data information converted
	X,Y,Z float64
	//Connection information
	Connection *i2c.I2C
	Address uint8
	Error error
}

type SelfTest struct{
	GyroX, GyroY, GyroZ int8
	AccX, AccY, AccZ uint8
}


//Device address
const(
 	DeviceAddressL uint8 = 0x68
	DeviceAddressH uint8 = 0x69
)

//Registers from MPU6050 register map v4.2
const(
	self_test_x int = 0x0d
	self_test_y int = 0x0e
	self_test_z int = 0x0f
	self_test_a int = 0x10
	smplrt_div int = 0x19
	config int = 0x1a
	gyro_config int = 0x1b
	accel_config int = 0x1c
	fifo_en int = 0x23
	i2c_mst_ctrl int = 0x24
	i2c_slv0_addr int = 0x25
	i2c_slv0_reg int = 0x26
	i2c_slv0_ctrl int = 0x27
	i2c_slv1_addr int = 0x28
	i2c_slv1_reg int = 0x29
	i2c_slv1_ctrl int = 0x2a
	i2c_slv2_addr int = 0x2b
	i2c_slv2_reg int = 0x2c
	i2c_slv2_ctrl int = 0x2d
	i2c_slv3_addr int = 0x2e
	i2c_slv3_reg int = 0x2f
	i2c_slv3_ctrl int = 0x30
	i2c_slv4_addr int = 0x31
	i2c_slv4_reg int = 0x32
	i2c_slv4_do int = 0x33
	i2c_slv4_ctrl int = 0x34
	i2c_slv4_di int = 0x35
	i2c_mst_status int = 0x36
	int_pin_cfg int = 0x37
	int_enable int = 0x38
	int_status int = 0x3a
	accel_xout_h int = 0x3b
	accel_xout_l int = 0x3c
	accel_yout_h int = 0x3d
	accel_yout_l int = 0x3e
	accel_zout_h int = 0x3f
	accel_zout_l int = 0x40
	temp_out_h int = 0x41
	temp_out_l int = 0x42
	gyro_xout_h int = 0x43
	gyro_xout_l int = 0x44
	gyro_yout_h int = 0x45
	gyro_yout_l int = 0x46
	gyro_zout_h int = 0x47
	gyro_zout_l int = 0x48
	ext_sens_data_00 int = 0x49
	ext_sens_data_01 int = 0x4a
	ext_sens_data_02 int = 0x4b
	ext_sens_data_03 int = 0x4c
	ext_sens_data_04 int = 0x4d
	ext_sens_data_05 int = 0x4e
	ext_sens_data_06 int = 0x4f
	ext_sens_data_07 int = 0x50
	ext_sens_data_08 int = 0x51
	ext_sens_data_09 int = 0x52
	ext_sens_data_10 int = 0x53
	ext_sens_data_11 int = 0x54
	ext_sens_data_12 int = 0x55
	ext_sens_data_13 int = 0x56
	ext_sens_data_14 int = 0x57
	ext_sens_data_15 int = 0x58
	ext_sens_data_16 int = 0x59
	ext_sens_data_17 int = 0x5a
	ext_sens_data_18 int = 0x5b
	ext_sens_data_19 int = 0x5c
	ext_sens_data_20 int = 0x5d
	ext_sens_data_21 int = 0x5e
	ext_sens_data_22 int = 0x5f
	ext_sens_data_23 int = 0x60
	i2c_slv0_do int = 0x63
	i2c_slv1_do int = 0x64
	i2c_slv2_do int = 0x65
	i2c_slv3_do int = 0x66
	i2c_mst_delay_ctrl int = 0x67
	signal_path_reset int = 0x68
	user_ctrl int = 0x6a
	pwr_mgmt_1 int = 0x6b
	pwr_mgmt_2 int = 0x6c
	fifo_counth int = 0x72
	fifo_countl int = 0x73
	fifo_r_w int = 0x74
	who_am_i int = 0x75
)


type selftestData struct{
	stdX, stdY,stdZ, stdA int16
}

type rangebounds struct {
	b1, b2 float64
}

func conversion(x, y rangebounds, n float64) float64{
	return y.b1 + (n - x.b1) * (y.b2 - y.b1) / (x.b2 - x.b1)
}

func (e MPU6050) ChangeDeviceAddress(LowValue bool){
	if LowValue == true{
		e.Address = DeviceAddressL
	} else {
	 	e.Address = DeviceAddressH
	}
}




func (e MPU6050) ConfigDLPF (){

}

func (e MPU6050)Start(bus int){
	if bus == 0{
		bus = 1
	}
	e.Connection, e.Error = i2c.NewI2C(e.Address,bus)
}

func (e MPU6050) Stop (){
	e.Connection.Close()
}